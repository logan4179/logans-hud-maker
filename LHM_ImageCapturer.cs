using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LHM_ImageCapturer : MonoBehaviour
{
	[Tooltip("Assign the rect transform you want to capture to this variable")] 
	public RectTransform RectTransform_ToCapture;

	public string ImageName = "capture";
	string rootFilePath = $"{Application.dataPath}/LHMCaptures/";

	[ContextMenu("z_ call CaptureToPNG()")]
	public void CallCaptureToPNG()
	{
		StartCoroutine( CaptureToPNG() );
	}

	public IEnumerator CaptureToPNG()
	{
		Debug.Log( $"calling {nameof(CaptureToPNG)}()..." );
		yield return new WaitForEndOfFrame(); // it must be a coroutine 

		if( RectTransform_ToCapture == null )
		{
			Debug.LogError( $"ERROR! You need to supply a rect transform to capture an image." );
		}
		else if( File.Exists($"{rootFilePath}{ImageName}.png") )
		{
			Debug.LogError( $"ERROR! File already exists. This capturer doesn't overwrite files as a precaution. Either " +
				$"delete the existing file, or change the name of the file you're saving." );
		}
		else
		{
			int rectWidth = System.Convert.ToInt32(RectTransform_ToCapture.rect.width);
			int rectHeight = System.Convert.ToInt32(RectTransform_ToCapture.rect.height);

			float startX = RectTransform_ToCapture.transform.position.x - rectWidth / 2;
			float startY = RectTransform_ToCapture.transform.position.y - rectHeight / 2;

			Texture2D tex = new Texture2D( rectWidth, rectHeight, TextureFormat.RGB24, false );
			tex.ReadPixels( new Rect(startX, startY, rectWidth, rectHeight), 0, 0 );
			tex.Apply();

			// Encode texture into PNG
			byte[] textureBytes = tex.EncodeToPNG();

			//When not in play mode, Destroy() will cause an error, but DestroyImmediate() will not...
			if( !Application.isPlaying )
			{
				DestroyImmediate( tex );
			}
			else
			{
				Destroy(tex);
			}

			/*
			if( !Directory.Exists(rootFilePath) )
			{
				Directory.CreateDirectory(rootFilePath);
				Debug.Log($"root file path did not exist, created folder...");

			}*/

			File.WriteAllBytes( $"{rootFilePath}{ImageName}.png", textureBytes );
		}

	}

	[ContextMenu("z_ call TakeScreenShot()")]
	public void TakeScreenShot()
	{
		Debug.Log($"calling {nameof(TakeScreenShot)}()...");

		if ( File.Exists($"{rootFilePath}{ImageName}.png") )
		{
			Debug.LogError($"ERROR! File already exists. This capturer doesn't overwrite files as a precaution. Either " +
				$"delete the existing file, or change the name of the file you're saving.");
		}
		else
		{
			ScreenCapture.CaptureScreenshot($"{rootFilePath}{ImageName}.png");

		}
	}
}
