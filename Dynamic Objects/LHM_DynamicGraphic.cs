using System;
using UnityEngine;
using UnityEngine.UI;


namespace LHM
{
	[RequireComponent(typeof(CanvasRenderer))]
	public class LHM_DynamicGraphic : Graphic
	{
		[Space(20f)]
		//public Graphic MyGraphic;
		private Canvas myCanvas;
		protected Camera myCamera;
		private float boundX;
		private float boundY;

		[Header("TARGETING")]
		public Vector3 FollowOffset;

		[Header("MOVEMENT")]
		public LHM_MovementMode MovementMode;
		public float MovementSpeed;

		[Header("ROTATION")]
		public LHM_RotationMode RotationMode;
		public LHM_RotationDirection RotationDirection;
		public float RotationSpeed;

		/// <summary>
		/// Causes this graphic to disappear if the player is looking towards it within threshold.
		/// </summary>
		public bool DisappearBasedOnFacing = false;
		[Tooltip("Threshold from 0 to 1 withwitch to make this graphic disappear if a supplied transform is facing. A value of 1 means the transform must face perfectly."), Range(-1f, 1f)]
		public float Threshold_DisappearBasedOnFacing = 0f;
		protected bool visibleOnLastFrame = true;

		//[Header("OTHER")]
		/// <summary>
		/// Forces this graphic to stay on the canvas.
		/// </summary>
		public bool ForceStayOnCanvas = false;
		/// <summary>
		/// Allows a Mathf.Abs() offset to keep the rect transform a certain amount within the canvas bounds
		/// </summary>
		public Vector2 V_ForceStayOnCanvas_Offset;


		public string DiagString;

		public void Initialize( Canvas canvas, Camera camera )
		{
			myCanvas = canvas;
			boundX = canvas.GetComponent<RectTransform>().rect.xMax;
			boundY = canvas.GetComponent<RectTransform>().rect.yMax;

			Rect canvasRect = canvas.GetComponent<RectTransform>().rect;
			print($"xMax: '{canvasRect.xMax}', xMin: '{canvasRect.xMin}'\n" +
				$"yMax: '{canvasRect.yMax}', yMin: '{canvasRect.yMin}'");

			myCamera = camera;


			visibleOnLastFrame = false;
		}


		public bool DiagMove = false;
		public bool DiagConversion = false;

		public void UpdateMe( float timeMult, Vector3 worldPosition )
		{
			UpdateRotation( timeMult );
			FollowWorldPosition( timeMult, worldPosition );
			//if( myCamera.WorldToViewportPoint(worldPosition).z;

			if( DisappearBasedOnFacing && 
				Vector3.Dot(myCamera.transform.forward, Vector3.Normalize(worldPosition - myCamera.transform.position)) > Threshold_DisappearBasedOnFacing )
			{
				if( visibleOnLastFrame )
				{
					//print("setting dirty");
					SetVerticesDirty();
				}
				visibleOnLastFrame = false;
				DiagString += "Should disappear";
			}
			else
			{
				if ( !visibleOnLastFrame )
				{
					//print("setting dirty");

					SetVerticesDirty();
				}
				visibleOnLastFrame = true;
			}
		}

		public void UpdateRotation( float timeMult )
		{
			if (RotationMode != LHM_RotationMode.None && RotationDirection != LHM_RotationDirection.None &&
				Mathf.Abs(RotationSpeed) > 0f)
			{
				rectTransform.Rotate(Vector3.back, RotationSpeed * timeMult);
				DiagString += "Currently Rotating.\n";
			}
		}

		public void FollowWorldPosition( float timeMult, Vector3 worldPosition )
		{
			DiagString = $"boundX: '{boundX}', boundY: '{boundY}'\n";

			if ( myCamera != null )
			{
				//this runs from 0 at the lower boundaries of the canvas, to (almost; about 80% of) the resolution value at the upper boundaries of the canvas
				//Vector3 v_wrldToScrnPt = myCamera.WorldToScreenPoint( worldPosition );

				//this runs 0 to 1, with 0.5 in middle
				Vector3 v_wrldToVwPrtPt = myCamera.WorldToViewportPoint(worldPosition); 

				//DiagString += $"WorldToScreenPoint: '{v_wrldToScrnPt}'\n";
				DiagString += $"WorldToViewportPoint: '{v_wrldToVwPrtPt}'\n";

				Vector2 anchoredEndGoal = new Vector2(-boundX + (v_wrldToVwPrtPt.x * 2f * boundX) + (FollowOffset.x), -boundY + (v_wrldToVwPrtPt.y * 2f * boundY) + FollowOffset.y); //this produces good results

				#region CORRECT POSITIONING----------------------------------------------------
				if( v_wrldToVwPrtPt.z < 0 )
				{
					//calculatedAnchoredGoal.x = -calculatedAnchoredGoal.x;
					//calculatedAnchoredGoal.x = Mathf.Sign(calculatedAnchoredGoal.x) * boundX;
					anchoredEndGoal.x = -Mathf.Sign(anchoredEndGoal.x) * boundX;

					//calculatedAnchoredGoal.y = -calculatedAnchoredGoal.y;
					anchoredEndGoal.y = -Mathf.Sign(anchoredEndGoal.y) * boundY;

					//calculatedAnchoredGoal = -calculatedAnchoredGoal;
					DiagString += $"BEHIND\n";

				}

				if ( ForceStayOnCanvas )
				{
					if( anchoredEndGoal.x > boundX )
					{
						anchoredEndGoal.x = boundX;
					}
					else if ( anchoredEndGoal.x < -boundX )
					{
						anchoredEndGoal.x = -boundX;
					}

					if ( anchoredEndGoal.y > boundY )
					{
						anchoredEndGoal.y = boundY;
					}
					else if (anchoredEndGoal.y < -boundY)
					{
						anchoredEndGoal.y = -boundY;
					}
				}
				#endregion

				if (MovementMode == LHM_MovementMode.Instantaneous)
				{
					rectTransform.anchoredPosition = anchoredEndGoal;
				}
				else if (MovementMode == LHM_MovementMode.Linear)
				{
					Vector3 vTo = anchoredEndGoal - rectTransform.anchoredPosition;
					if ( vTo.magnitude < MovementSpeed * timeMult )
					{
						rectTransform.anchoredPosition = anchoredEndGoal;
					}
					else
					{
						rectTransform.Translate( vTo.normalized * MovementSpeed * timeMult, Space.World );
					}
				}
				else if (MovementMode == LHM_MovementMode.Lerped)
				{
					rectTransform.anchoredPosition = Vector2.Lerp(
						rectTransform.anchoredPosition, anchoredEndGoal, MovementSpeed * timeMult
					);
				}

				DiagString += $"anchoredEndGoal: '{anchoredEndGoal}'\n";
				DiagString += $"rectTransform.anchoredPosition: '{rectTransform.anchoredPosition}'\n";
				DiagString += $"rectTransform.position: '{rectTransform.position}'\n";

			}
		}
	}
}