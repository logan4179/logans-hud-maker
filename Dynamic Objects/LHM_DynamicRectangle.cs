using UnityEngine;
using UnityEngine.UI;

namespace LHM
{
	public class LHM_DynamicRectangle : LHM_DynamicGraphic
	{
		[Header("RECTANGLE STATS")]
		public Vector3 CenterPosition;
		public float Height;
		public float Width;

		protected override void OnPopulateMesh( VertexHelper vh )
		{
			//print( "graphicrectangle.OnPopulateMesh rect" );
			vh.Clear();
			if (!visibleOnLastFrame)
			{
				return;
			}

			Vector3[] crnrs = GenerateCorners();
			DrawRectangle_absolutePositioning( crnrs, vh );

		}

		void DrawRectangle_absolutePositioning(Vector3[] crnrs, VertexHelper vh)
		{
			UIVertex vtx = UIVertex.simpleVert;
			vtx.color = color;

			//top-left
			vtx.position = crnrs[0];
			vtx.color = color;
			vh.AddVert(vtx);

			//top-left
			vtx.position = crnrs[1];
			vtx.color = color;
			vh.AddVert(vtx);

			//top-left
			vtx.position = crnrs[2];
			vtx.color = color;
			vh.AddVert(vtx);

			//top-left
			vtx.position = crnrs[3];
			vtx.color = color;
			vh.AddVert(vtx);

			vh.AddTriangle(0, 1, 2);
			vh.AddTriangle(2, 3, 0);

		}

		public Vector3[] GenerateCorners()
		{
			return LHM_Utilities.GenerateRectCorners( CenterPosition, Height, Width );
		}

	}
}