using UnityEngine;
using UnityEngine.UI;

namespace LHM
{
	public class LHM_DynamicRectangularFrame : LHM_DynamicGraphic
	{
		[Header("RECTANGULAR FRAME")]
		private int vertexIndex = 0; //this was necessary once I started drawing more than one shape...
		public float Height;
		public float Width;
		public float LineThickness;


		private Vector3[] Corners_TopRect;

		private Vector3[] Corners_BottomRect;

		private Vector3[] Corners_LeftRect;

		private Vector3[] Corners_RightRect;


		protected override void OnPopulateMesh( VertexHelper vh )
		{
			vh.Clear();

			//print("OnPopulateMesh frame");

			if( !visibleOnLastFrame )
			{
				return;
			}
			vertexIndex = 0;

			GenerateCorners();
			//DrawRectangle_absolutePositioning(Corners_TopRect, vh);
			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_TopRect, color, vh, ref vertexIndex);

			//DrawRectangle_absolutePositioning(Corners_RightRect, vh);
			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_RightRect, color, vh, ref vertexIndex);

			//DrawRectangle_absolutePositioning(Corners_BottomRect, vh);
			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_BottomRect, color, vh, ref vertexIndex);

			//DrawRectangle_absolutePositioning(Corners_LeftRect, vh);
			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_LeftRect, color, vh, ref vertexIndex);
			//print("got to end");
		}

		public void GenerateCorners()
		{
			Corners_TopRect = LHM_Utilities.GenerateRectangularFrameCorners(
				LineThickness, Height, Width, 0
			);

			Corners_RightRect = LHM_Utilities.GenerateRectangularFrameCorners(
				 LineThickness, Height, Width, 1
			);

			Corners_BottomRect = LHM_Utilities.GenerateRectangularFrameCorners(
				LineThickness, Height, Width, 2
			);
			Corners_LeftRect = LHM_Utilities.GenerateRectangularFrameCorners(
				LineThickness, Height, Width, 3
			);

		}
	}
}