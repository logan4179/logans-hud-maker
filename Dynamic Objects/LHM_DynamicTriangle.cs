using UnityEngine;
using UnityEngine.UI;

namespace LHM
{
	public class LHM_DynamicTriangle : LHM_DynamicGraphic
	{
		[Header("TRIANGLE")]
		public float Height;
		public float Width;
		private Vector3[] Corners;

		protected override void OnPopulateMesh( VertexHelper vh )
		{
			vh.Clear();
			//print("tri.onpopulatemesh()");
			if (!visibleOnLastFrame)
			{
				return;
			}

			Corners = LHM_Utilities.GenerateIsoscelesTriCorners(Height, Width); 
			LHM_Utilities.DrawTriangle_absolutePositioning( Corners, color, vh );
		}
	}
}