using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace LHM
{
	[RequireComponent(typeof(CanvasRenderer))]
	public class LHM_PlottedLine : Graphic
	{
		public Vector2[] LinePoints;

		private Vector2[] wallPoints_sideA;
		private Vector2[] wallPoints_sideB;

		public float LineThickness = 1f;

		private int vertexIndex = 0; //this is necessary in order to draw more than one shape...

		//Debug...
		public float[] DebugBendAngles;
		public Vector2[] DebugCorners;
		public float[] DebugCornerMagnitudes;
		public bool DrawGeometry = true;

		protected override void OnPopulateMesh(VertexHelper vh)
		{
			//print("OnPopulateMesh plotted line");\
			//print($"transform.position: '{transform.position}', rt.position: '{rectTransform.position}'");
			vh.Clear();
			vertexIndex = 0;

			if( LinePoints == null || LinePoints.Length <= 1 )
			{
				return;
			}

			GenerateAllCorners();

			if( DrawGeometry )
			{
				for ( int i = 0; i < LinePoints.Length - 1; i++ )
				{
					LHM_Utilities.DrawRectangle_absolutePositioning( 
						wallPoints_sideA[i], 
						wallPoints_sideA[i + 1], 
						wallPoints_sideB[i + 1], 
						wallPoints_sideB[i], 
						color, vh, ref vertexIndex 
						);
				}
			}

		}


		[ContextMenu("z call GenerateAllCorners()")]
		private void GenerateAllCorners()
		{
			//print($"{nameof(LHM_PlottedLine_v2)}.{nameof(GenerateAllCorners)}()");
			if( LinePoints == null )
			{
				return;
			}

			wallPoints_sideA = new Vector2[LinePoints.Length];
			wallPoints_sideB = new Vector2[LinePoints.Length];
			DebugBendAngles = new float[LinePoints.Length];
			DebugCorners = new Vector2[LinePoints.Length];
			DebugCornerMagnitudes = new float[LinePoints.Length];

			// First walls...
			Vector2 vTo = LinePoints[1] - LinePoints[0];
			wallPoints_sideA[0] =  LinePoints[0] + 
				(Vector2)( Quaternion.Euler(0f, 0f, 90) * (vTo.normalized * LineThickness * 0.5f) );
			wallPoints_sideB[0] =  LinePoints[0] +
				(Vector2)( Quaternion.Euler(0f, 0f, -90) * (vTo.normalized * LineThickness * 0.5f) );

			// Last walls...
			vTo = LinePoints[LinePoints.Length-2] - LinePoints[LinePoints.Length - 1];
			wallPoints_sideA[LinePoints.Length-1] = LinePoints[LinePoints.Length-1] + 
				(Vector2)( Quaternion.Euler(0f, 0f, -90f) * (vTo.normalized * LineThickness * 0.5f) );
			wallPoints_sideB[LinePoints.Length - 1] = LinePoints[LinePoints.Length - 1] +
				(Vector2)( Quaternion.Euler(0f, 0f, 90f) * (vTo.normalized * LineThickness * 0.5f) );

			// Intermediate Walls...
			for ( int i = 1; i < LinePoints.Length - 1; i++ )
			{
				float bendAngle = LHM_Utilities.SignedAngleAtBend( LinePoints[i - 1], LinePoints[i], LinePoints[i + 1] );
				if( bendAngle == 180f )
				{
					bendAngle *= -1f; //Found out I need to do this because the sign basically flips when it reaches 180, creating an issue where wall points can flip if I don't do this...
				}
				DebugBendAngles[i] = bendAngle;

				Vector2 v_corner = LHM_Utilities.CornerProject(LinePoints[i - 1], LinePoints[i], LinePoints[i + 1]);
				DebugCorners[i] = v_corner;

				DebugCornerMagnitudes[i] = v_corner.magnitude;

				float hypotenuseLength = LHM_Utilities.GetHypotenuseLengthViaSOH(Mathf.Abs(bendAngle) / 2f, LineThickness * 0.5f);

				wallPoints_sideA[i] = LinePoints[i] + (v_corner * hypotenuseLength * Mathf.Sign(bendAngle));
				wallPoints_sideB[i] = LinePoints[i] + (v_corner * hypotenuseLength * -Mathf.Sign(bendAngle));
			}
		}

		public void ClearMe()
		{
			LinePoints = null;
			SetVerticesDirty();
		}

		public bool DrawMyGizmos;

#if UNITY_EDITOR
		public Color Color_sideA = Color.red;
		public Color Color_sideB = Color.blue;
		private void OnDrawGizmos()
		{
			if( !DrawMyGizmos )
			{
				return;
			}

			if( LinePoints == null || LinePoints.Length <= 0 )
			{
				return;
			}

			for( int i = 0; i < LinePoints.Length; i++ )
			{
				//Handles.Label( rectTransform.position + LinePoints[i], i.ToString() ); //v3
				Handles.Label( (Vector2)rectTransform.position + LinePoints[i], i.ToString());

				if ( i > 0 )
				{
					
					Gizmos.DrawLine( (Vector2)rectTransform.position + LinePoints[i-1], (Vector2)rectTransform.position + LinePoints[i] );


					//Gizmos.DrawLine((Vector3)rectTransform.anchoredPosition + LinePoints[i - 1], (Vector3)rectTransform.anchoredPosition + LinePoints[i]);

					//Gizmos.color = Color_sideA;
					//Gizmos.DrawLine(rectTransform.position + wallPoints_sideA[i - 1], rectTransform.position + wallPoints_sideA[i]);

					//Gizmos.color = Color_sideB;
					//Gizmos.DrawLine(rectTransform.position + wallPoints_sideB[i - 1], rectTransform.position + wallPoints_sideB[i]);
				}
			}

			
			if( wallPoints_sideA != null )
			{
				Gizmos.color = Color_sideA;

				for ( int i = 0; i < wallPoints_sideA.Length; i++ )
				{
					Handles.Label( (Vector2)rectTransform.position + wallPoints_sideA[i], "A" + i.ToString() );

					if (i > 0)
					{
						Gizmos.DrawLine( (Vector2)rectTransform.position + wallPoints_sideA[i - 1], (Vector2)rectTransform.position + wallPoints_sideA[i]);

						//Gizmos.color = Color_sideA;
						//Gizmos.DrawLine(rectTransform.position + wallPoints_sideA[i - 1], rectTransform.position + wallPoints_sideA[i]);
					}
				}
			}

			if( wallPoints_sideB != null )
			{			
				Gizmos.color = Color_sideB;
				for ( int i = 0; i < wallPoints_sideB.Length; i++ )
				{
					Handles.Label( (Vector2)rectTransform.position + wallPoints_sideB[i], "B" + i.ToString() );

					if (i > 0)
					{
						Gizmos.DrawLine( (Vector2)rectTransform.position + wallPoints_sideB[i - 1], (Vector2)rectTransform.position + wallPoints_sideB[i]);

						//Gizmos.color = Color_sideA;
						//Gizmos.DrawLine(rectTransform.position + wallPoints_sideA[i - 1], rectTransform.position + wallPoints_sideA[i]);

						//Gizmos.color = Color_sideB;
						//Gizmos.DrawLine(rectTransform.position + wallPoints_sideB[i - 1], rectTransform.position + wallPoints_sideB[i]);
					}
				}
			}
		}
#endif
	}
}