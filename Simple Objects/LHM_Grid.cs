using UnityEngine;
using UnityEngine.UI;

namespace LHM
{
	[RequireComponent(typeof(CanvasRenderer))]
	public class LHM_Grid : Graphic
	{
		[Header("FRAME")]
		public float FrameThickness = 5f;

		[Header("GRID")]
		public bool DrawGridLines = true;
		public float GridLineThickness = 3f;

		[Space(20f)]
		public bool DrawVerticalGridLines = true;
		public int VerticalLineCount = 10;
		public Color VerticalLinesColor = new Color(1f, 1f, 1f, 0.5f);

		[SerializeField] private Vector2[] verticalLineStartPoints;
		/// <summary>
		/// position data for where vertical grid lines start (lowest point)
		/// </summary>
		public Vector2[] VerticalLineStartPoints => verticalLineStartPoints;
		private Vector2[] verticalLineEndPoints;
		/// <summary>
		/// position data for where vertical grid lines end (highest point)
		/// </summary>
		public Vector2[] VerticalLineEndPoints => verticalLineEndPoints;
		private Vector2[] verticalLineLowerBounds;
		private Vector2[] verticalLineUpperBounds;

		[Space(20f)]
		public bool DrawHorizontalGridLines = true;
		public int HorizontalLineCount = 10;
		public Color HorizontalLinesColor = new Color(1f, 1f, 1f, 0.5f);

		private Vector2[] horizontalLineStartPoints;
		/// <summary>
		/// position data for where horizontal grid lines start (left-most point)
		/// </summary>
		public Vector2[] HorizontalLineStartPoints => horizontalLineStartPoints;
		private Vector2[] horizontalLineEndPoints;
		/// <summary>
		/// position data for where vertical grid lines end (right-most point)
		/// </summary>
		public Vector2[] HorizontalLineEndPoints => horizontalLineEndPoints;
		private Vector2[] horizontalLineLowerBounds;
		private Vector2[] horizontalLineUpperBounds;


		private Vector3[] Corners_TopRect;

		private Vector3[] Corners_BottomRect;

		private Vector3[] Corners_LeftRect;

		private Vector3[] Corners_RightRect;

		[Header("OTHER")]

		int vertexIndex = 0;


		//[Header("DEBUG")]



		protected override void OnPopulateMesh(VertexHelper vh)
		{
			//print("OnPopulateMesh grid");

			vh.Clear();
			vertexIndex = 0;

			GenerateAllCorners();

			LHM_Utilities.DrawRectangle_absolutePositioning( Corners_TopRect, color, vh, ref vertexIndex );
			LHM_Utilities.DrawRectangle_absolutePositioning( Corners_RightRect, color, vh, ref vertexIndex );
			LHM_Utilities.DrawRectangle_absolutePositioning( Corners_BottomRect, color, vh, ref vertexIndex );
			LHM_Utilities.DrawRectangle_absolutePositioning( Corners_LeftRect, color, vh, ref vertexIndex );

			if( DrawGridLines )
			{
				if( DrawVerticalGridLines )
				{
					for ( int i = 0; i < VerticalLineCount; i++ )
					{
						LHM_Utilities.DrawRectangle_absolutePositioning(
							verticalLineLowerBounds[i], 
							verticalLineUpperBounds[i], 
							VerticalLinesColor, vh, ref vertexIndex 
							);
					}
				}

				if( DrawHorizontalGridLines )
				{
					for ( int i = 0; i < HorizontalLineCount; i++ )
					{
						LHM_Utilities.DrawRectangle_absolutePositioning(
							horizontalLineLowerBounds[i],
							horizontalLineUpperBounds[i],
							HorizontalLinesColor, vh, ref vertexIndex
							);
					}
				}
			}
		}

		private void GenerateAllCorners()
		{
			Corners_TopRect = LHM_Utilities.GenerateRectangularFrameCorners(
			FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 0
			);

			Corners_RightRect = LHM_Utilities.GenerateRectangularFrameCorners(
				FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 1
				);

			Corners_BottomRect = LHM_Utilities.GenerateRectangularFrameCorners(
				FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 2
				);
			Corners_LeftRect = LHM_Utilities.GenerateRectangularFrameCorners(
				FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 3
				);


			if( VerticalLineCount > 0 )
			{
				verticalLineStartPoints = new Vector2[VerticalLineCount];
				verticalLineEndPoints = new Vector2[VerticalLineCount];
				verticalLineLowerBounds = new Vector2[VerticalLineCount];
				verticalLineUpperBounds = new Vector2[VerticalLineCount];
				float cellWidth = rectTransform.rect.width / (VerticalLineCount + 1);
				//print($"cellwidth calculated to: '{cellWidth}'");

				for (int i = 0; i < VerticalLineCount; i++)
				{
					verticalLineStartPoints[i] = new Vector2(
						(-rectTransform.rect.width * 0.5f) + ((i + 1) * cellWidth),
						(-rectTransform.rect.height * 0.5f) + (0.5f * FrameThickness)
						);
					verticalLineLowerBounds[i] = new Vector2(
						verticalLineStartPoints[i].x - (GridLineThickness * 0.5f),
						verticalLineStartPoints[i].y
						);

					verticalLineEndPoints[i] = new Vector2(
						(-rectTransform.rect.width * 0.5f) + ((i + 1) * cellWidth),
						(rectTransform.rect.height * 0.5f) - (0.5f * FrameThickness)
						);

					verticalLineUpperBounds[i] = new Vector2(
						verticalLineEndPoints[i].x + (GridLineThickness * 0.5f),
						verticalLineEndPoints[i].y
						);
				}
			}

			if ( HorizontalLineCount > 0 )
			{
				horizontalLineStartPoints = new Vector2[HorizontalLineCount];
				horizontalLineEndPoints = new Vector2[HorizontalLineCount];
				horizontalLineLowerBounds = new Vector2[HorizontalLineCount];
				horizontalLineUpperBounds = new Vector2[HorizontalLineCount];
				float cellHeight = rectTransform.rect.height / (HorizontalLineCount + 1);

				for ( int i = 0; i < HorizontalLineCount; i++ )
				{
					horizontalLineStartPoints[i] = new Vector2(
						(-rectTransform.rect.width * 0.5f) + (0.5f * FrameThickness),
						(-rectTransform.rect.height * 0.5f) + ((i+1) * cellHeight)
						);
					horizontalLineLowerBounds[i] = new Vector2(
						horizontalLineStartPoints[i].x,
						horizontalLineStartPoints[i].y -+ (GridLineThickness * 0.5f)
						);

					horizontalLineEndPoints[i] = new Vector2(
						(rectTransform.rect.width * 0.5f) - (0.5f * FrameThickness),
						(-rectTransform.rect.height * 0.5f) + ((i + 1) * cellHeight)
						);

					horizontalLineUpperBounds[i] = new Vector2(
						horizontalLineEndPoints[i].x,
						horizontalLineEndPoints[i].y + (GridLineThickness * 0.5f)
						);
				}
			}
		}

		#region API -------------------------------------------------//////////////////////////////
		public void AlignVerticalUIDelineators(bool originsAlignedWithGrid, bool alignWithStartPoint, Vector2 offset, params RectTransform[] rts )
		{
			if( VerticalLineCount <= 0 )
			{
				Debug.LogError( $"LHM ERROR! Can't call {nameof(AlignVerticalUIDelineators)} with a {nameof(VerticalLineCount)} of {VerticalLineCount}" );
				return;
			}

			for ( int i = 0; i < rts.Length; i++ )
			{
				if( i < horizontalLineStartPoints.Length )
				{
					//rts[i].position = horizontalLineStartPoints[i];
					if( originsAlignedWithGrid )
					{
						rts[i].anchoredPosition = (alignWithStartPoint ? horizontalLineStartPoints[i] : horizontalLineEndPoints[i]) + offset;
					}
					else
					{
						//rts[i].position = rectTransform.anchoredPosition + horizontalLineStartPoints[i];
						rts[i].position = (Vector2)rectTransform.position + (alignWithStartPoint ? horizontalLineStartPoints[i] : horizontalLineEndPoints[i]) + offset;

					}
				}
			}
		}

		public void AlignHorizontalUIDelineators( bool parentedToGrid, bool alignWithStartPoint, Vector2 offset, params RectTransform[] rts )
		{
			if ( HorizontalLineCount <= 0 )
			{
				Debug.LogError($"LHM ERROR! Can't call {nameof(AlignHorizontalUIDelineators)} with a {nameof(HorizontalLineCount)} of {HorizontalLineCount}");
				return;
			}

			for( int i = 0; i < rts.Length; i++ )
			{
				if ( i < verticalLineStartPoints.Length )
				{
					//rts[i].position = horizontalLineStartPoints[i];
					if ( parentedToGrid )
					{
						rts[i].anchoredPosition = (alignWithStartPoint ? verticalLineStartPoints[i] : verticalLineEndPoints[i]) + offset;
					}
					else
					{
						//rts[i].position = rectTransform.anchoredPosition + horizontalLineStartPoints[i];
						rts[i].position = (Vector2)rectTransform.position + (alignWithStartPoint ? verticalLineStartPoints[i] : verticalLineEndPoints[i]) + offset;

					}
				}
			}
		}

		public void PlotLine_normalized( LHM_PlottedLine line, params Vector2[] points )
		{
			line.LinePoints = new Vector2[points.Length];

			for ( int i = 0; i < points.Length; i++ )
			{
				line.LinePoints[i] = rectTransform.rect.min +
					new Vector2(points[i].x * rectTransform.rect.width, points[i].y * rectTransform.rect.height);
			}

			line.SetVerticesDirty();
		}

		public void PlotLine_absolute( LHM_PlottedLine line, float maxGraphDomain, float maxGraphRange, params Vector2[] points )
		{
			//print($"Pl6otLine_absolute(). rect min: '{rectTransform.rect.min}', rect width: '{rectTransform.rect.width}'");
			line.LinePoints = new Vector2[points.Length];

			for ( int i = 0; i < points.Length; i++ )
			{
				line.LinePoints[i] = rectTransform.rect.min +
					new Vector2( 
						(points[i].x / maxGraphDomain) * rectTransform.rect.width, 
						(points[i].y / maxGraphRange) * rectTransform.rect.height 
						);
			}

			line.SetVerticesDirty();
		}

		#endregion

		public bool DrawMyGizmos;
		[TextArea(1, 5)] public string debugString;
		private void OnDrawGizmos()
		{
			debugString = $"pos: '{transform.position}'\n" +
				$"lclpos: '{transform.localPosition}'\n" +
				$"rt.pos: '{rectTransform.position}'\n" +
				$"rt.anchoredpos: '{rectTransform.anchoredPosition}'\n" +
				$"rect min: '{rectTransform.rect.min}'";

			if( !DrawMyGizmos )
			{
				return;
			}

			Gizmos.DrawCube(transform.position, Vector3.one * 10f);
			/*
			if( verticalLineStartPoints != null && verticalLineStartPoints.Length > 0 )
			{
				for( int i = 0; i < verticalLineStartPoints.Length; i++ )
				{
					Handles.Label(rectTransform.position + (Vector3)verticalLineStartPoints[i], $"v{i}");
				}
			}

			if ( horizontalLineStartPoints != null && horizontalLineStartPoints.Length > 0 )
			{
				for ( int i = 0; i < horizontalLineStartPoints.Length; i++ )
				{
					Handles.Label( rectTransform.position + (Vector3)horizontalLineStartPoints[i], $"h{i}" );
				}
			}
			*/

			/*
			LHM_Utilities.DrawCornerHandleLables( 
				rectTransform.position + new Vector3(verticalLineLowerBounds[tryIndex].x, verticalLineUpperBounds[tryIndex].y),
				rectTransform.position + (Vector3)verticalLineUpperBounds[tryIndex],
				rectTransform.position + new Vector3(verticalLineUpperBounds[tryIndex].x, verticalLineLowerBounds[tryIndex].y ),
				rectTransform.position + (Vector3)verticalLineLowerBounds[tryIndex] );
			*/
		}
		
	}
}