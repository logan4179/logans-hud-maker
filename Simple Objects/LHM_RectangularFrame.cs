using UnityEngine;
using UnityEngine.UI;

namespace LHM
{
	[RequireComponent(typeof(CanvasRenderer))]
	public class LHM_RectangularFrame : Graphic
	{
		[Header("RECTANGULAR FRAME")]
		public float Height = 100f;
		public float Width = 100f;
		public float FrameThickness = 10f;
		[Tooltip("When enabled, this makes the frame ignore the Height and Width setting, and instead scale up based on the size of the rect transform")] 
		public bool RectScaling = true;

		private Vector3[] Corners_TopRect;
		private Vector3[] Corners_BottomRect;
		private Vector3[] Corners_LeftRect;
		private Vector3[] Corners_RightRect; 
		private int vertexIndex = 0; //this was necessary once I started drawing more than one shape...

		protected override void OnPopulateMesh(VertexHelper vh)
		{
			//print("OnPopulateMesh frame");
			vh.Clear();
			vertexIndex = 0;

			GenerateAllCorners();

			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_TopRect, color, vh, ref vertexIndex);
			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_RightRect, color, vh, ref vertexIndex);
			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_BottomRect, color, vh, ref vertexIndex);
			LHM_Utilities.DrawRectangle_absolutePositioning(Corners_LeftRect, color, vh, ref vertexIndex);

		}

		public void GenerateAllCorners()
		{
			if( RectScaling )
			{
				Corners_TopRect = LHM_Utilities.GenerateRectangularFrameCorners(
					FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 0
					);

				Corners_RightRect = LHM_Utilities.GenerateRectangularFrameCorners(
					FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 1
					);

				Corners_BottomRect = LHM_Utilities.GenerateRectangularFrameCorners(
					FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 2
					);
				Corners_LeftRect = LHM_Utilities.GenerateRectangularFrameCorners(
					FrameThickness, rectTransform.rect.height - (0.5f * FrameThickness), rectTransform.rect.width - (0.5f * FrameThickness), 3
					);
			}
			else
			{
				Corners_TopRect = LHM_Utilities.GenerateRectangularFrameCorners( FrameThickness, Height, Width, 0 );
				Corners_RightRect = LHM_Utilities.GenerateRectangularFrameCorners( FrameThickness, Height, Width, 1 );
				Corners_BottomRect = LHM_Utilities.GenerateRectangularFrameCorners( FrameThickness, Height, Width, 2 );
				Corners_LeftRect = LHM_Utilities.GenerateRectangularFrameCorners( FrameThickness, Height, Width, 3 );
			}
		}
	}
}