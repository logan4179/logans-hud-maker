using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace LHM
{

	public class LHM_PlottedLine_old : Graphic
	{
		public Vector3[] LinePoints;

		[SerializeField] private Vector3[] wallPoints_sideA;
		[SerializeField] private Vector3[] wallPoints_sideB;

		public float LineThickness = 1f;

		private int vertexIndex = 0; //this is necessary in order to draw more than one shape...

		//Debug...
		public float[] DebugBendAngles;

		protected override void OnPopulateMesh(VertexHelper vh)
		{
			//print("OnPopulateMesh plotted line");\
			//print($"transform.position: '{transform.position}', rt.position: '{rectTransform.position}'");
			vh.Clear();
			vertexIndex = 0;

			GenerateAllCorners();

			//return;
			for ( int i = 0; i < LinePoints.Length - 1; i++ )
			{
				LHM_Utilities.DrawRectangle_absolutePositioning( 
					wallPoints_sideA[i], 
					wallPoints_sideA[i + 1], 
					wallPoints_sideB[i + 1], 
					wallPoints_sideB[i], 
					color, vh, ref vertexIndex 
					);
			}

		}

		public float deg = 45f;

		[ContextMenu("z call GenerateAllCorners()")]
		private void GenerateAllCorners()
		{
			wallPoints_sideA = new Vector3[LinePoints.Length];
			wallPoints_sideB = new Vector3[LinePoints.Length];
			DebugBendAngles = new float[LinePoints.Length];

			// First walls...
			Vector3 vTo = LinePoints[1] - LinePoints[0];
			wallPoints_sideA[0] =  LinePoints[0] + 
				(Quaternion.Euler(0f, 0f, 90) * (Vector3.Normalize(vTo) * LineThickness * 0.5f));
			wallPoints_sideB[0] =  LinePoints[0] +
				(Quaternion.Euler(0f, 0f, -90) * (Vector3.Normalize(vTo) * LineThickness * 0.5f));
			//LineAngles[0] = 0f;

			// Last walls...
			vTo = LinePoints[LinePoints.Length-2] - LinePoints[LinePoints.Length - 1];
			wallPoints_sideA[LinePoints.Length-1] = LinePoints[LinePoints.Length-1] + 
				(Quaternion.Euler(0f, 0f, -90f) * (Vector3.Normalize(vTo) * LineThickness * 0.5f));
			wallPoints_sideB[LinePoints.Length - 1] = LinePoints[LinePoints.Length - 1] +
				(Quaternion.Euler(0f, 0f, 90f) * (Vector3.Normalize(vTo) * LineThickness * 0.5f));
			//LineAngles[LinePoints.Length-1] = 0f;

			// Intermediate Walls...
			for ( int i = 1; i < LinePoints.Length - 1; i++ )
			{
				//print($"i.....................................................................");
				float bendAngle = LHM_Utilities.SignedAngleAtBend( LinePoints[i - 1], LinePoints[i], LinePoints[i + 1] );
				DebugBendAngles[i] = bendAngle;
				print( $"got angle: '{bendAngle}'" );
				Vector3 v_corner = LHM_Utilities.CornerProject( LinePoints[i - 1], LinePoints[i], LinePoints[i + 1] ) * LineThickness * 0.5f;

				wallPoints_sideA[i] = LinePoints[i] + ( v_corner * Mathf.Sign(bendAngle) );
				//wallPoints_sideA[i] = LinePoints[i] + (v_corner * Mathf.Sign(bendAngle) * LHM_Utilities.GetHypotenuseLength(bendAngle, LineThickness * 0.5f)); //nope
				//wallPoints_sideA[i] = LinePoints[i] + (v_corner * Mathf.Sign(bendAngle) * Mathf.Abs(LHM_Utilities.GetHypotenuseLength(bendAngle, LineThickness * 0.5f)) ); //nope

				wallPoints_sideB[i] = LinePoints[i] + ( v_corner * -Mathf.Sign(bendAngle) );
			}
		}

#if UNITY_EDITOR
		public Color Color_sideA, Color_sideB;
		private void OnDrawGizmos()
		{

			for( int i = 0; i < LinePoints.Length; i++ )
			{
				Handles.Label( rectTransform.position + LinePoints[i], i.ToString() );
				
				if ( i > 0 )
				{
					Gizmos.DrawLine( rectTransform.position + LinePoints[i-1], rectTransform.position + LinePoints[i] );
					//Gizmos.DrawLine(transform.position + LinePoints[i - 1], transform.position + LinePoints[i]);
					//Gizmos.DrawLine((Vector3)rectTransform.anchoredPosition + LinePoints[i - 1], (Vector3)rectTransform.anchoredPosition + LinePoints[i]);

					//Gizmos.color = Color_sideA;
					//Gizmos.DrawLine(rectTransform.position + wallPoints_sideA[i - 1], rectTransform.position + wallPoints_sideA[i]);

					//Gizmos.color = Color_sideB;
					//Gizmos.DrawLine(rectTransform.position + wallPoints_sideB[i - 1], rectTransform.position + wallPoints_sideB[i]);
				}
			}

			Gizmos.color = Color_sideA;
			for ( int i = 0; i < wallPoints_sideA.Length; i++ )
			{
				Handles.Label( rectTransform.position + wallPoints_sideA[i], "A" + i.ToString() );

				if (i > 0)
				{
					Gizmos.DrawLine(rectTransform.position + wallPoints_sideA[i - 1], rectTransform.position + wallPoints_sideA[i]);

					//Gizmos.color = Color_sideA;
					//Gizmos.DrawLine(rectTransform.position + wallPoints_sideA[i - 1], rectTransform.position + wallPoints_sideA[i]);
				}
			}

			Gizmos.color = Color_sideB;
			for ( int i = 0; i < wallPoints_sideB.Length; i++ )
			{
				Handles.Label( rectTransform.position + wallPoints_sideB[i], "B" + i.ToString() );

				if (i > 0)
				{
					Gizmos.DrawLine(rectTransform.position + wallPoints_sideB[i - 1], rectTransform.position + wallPoints_sideB[i]);

					//Gizmos.color = Color_sideA;
					//Gizmos.DrawLine(rectTransform.position + wallPoints_sideA[i - 1], rectTransform.position + wallPoints_sideA[i]);

					//Gizmos.color = Color_sideB;
					//Gizmos.DrawLine(rectTransform.position + wallPoints_sideB[i - 1], rectTransform.position + wallPoints_sideB[i]);
				}
			}
		}
#endif
	}
}