using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace LHM
{
	public enum LHM_MovementMode
	{
		Instantaneous,
		Linear,
		Lerped,
		Sine,
	}

	public enum LHM_RotationMode
	{
		None,
		Consistent,
	}

	public enum LHM_RotationDirection
	{
		None,
		Clockwise,
		CounterClockwise,
	}

	public static class LHM_Utilities
	{
		public static void DrawTriangle_absolutePositioning( Vector3[] crnrs, UnityEngine.Color col, VertexHelper vh )
		{
			UIVertex vtx = UIVertex.simpleVert;
			vtx.color = col;

			vtx.position = crnrs[0];
			vh.AddVert(vtx);

			vtx.position = crnrs[1];
			vh.AddVert(vtx);

			vtx.position = crnrs[2];
			vh.AddVert(vtx);


			vh.AddTriangle( 0, 1, 2 );
		}

		public static Vector3[] GenerateIsoscelesTriCorners(float ht, float wdth)
		{
			Vector3[] crnrs = new Vector3[3] {
				(Vector3.up * ht * 0.5f), //top
				(Vector3.down * ht * 0.5f) + (Vector3.right * wdth * 0.5f), //bottom-right
				(Vector3.down * ht * 0.5f) + (Vector3.left * wdth * 0.5f), //bottom-left

			};

			return crnrs;
		}
		public static Vector3[] GenerateIsoscelesTriCorners( Vector3 cntrPos_passed, float ht, float wdth )
		{
			Vector3[] crnrs = new Vector3[3] {
				cntrPos_passed + (Vector3.up * ht * 0.5f), //top
				cntrPos_passed + (Vector3.down * ht * 0.5f) + (Vector3.right * wdth * 0.5f), //bottom-right
				cntrPos_passed + (Vector3.down * ht * 0.5f) + (Vector3.left * wdth * 0.5f), //bottom-left

			};

			return crnrs;
		}
		
		public static Vector3[] GenerateRectCorners( Vector3 cntrPos_passed, float ht, float wdth )
		{
			Vector3[] crnrs = new Vector3[4] {
				cntrPos_passed + (Vector3.up * ht * 0.5f) + (Vector3.left * wdth * 0.5f), //top-left
				cntrPos_passed + (Vector3.up * ht * 0.5f) + (Vector3.right * wdth * 0.5f), //top-right
				cntrPos_passed + (Vector3.down * ht * 0.5f) + (Vector3.right * wdth * 0.5f), //bottom-right
				cntrPos_passed +(Vector3.down * ht * 0.5f) + (Vector3.left * wdth * 0.5f) //bottom-left

			};

			return crnrs;
		}

		public static Vector3[] GenerateLineCorners( Vector3 strtPos, Vector3 endPos, float thckNess )
		{
			Vector3 startToEnd = Vector3.Normalize( endPos - strtPos );

			Vector3[] crnrs = new Vector3[4] {
				strtPos + (Vector3.Cross(startToEnd, Vector3.back).normalized * thckNess * 0.5f), //top-left
				endPos + (Vector3.Cross(startToEnd, Vector3.back).normalized * thckNess * 0.5f), //top-right
				endPos + (Vector3.Cross(startToEnd, Vector3.forward).normalized * thckNess * 0.5f), //bottom-right
				strtPos + (Vector3.Cross(startToEnd, Vector3.forward).normalized * thckNess * 0.5f) //bottom-left
			};

			return crnrs;
		}

		public static Vector3[] GenerateMiteredLineCorners( Vector3 strtPos, Vector3 endPos, float thckNess )
		{
			Vector3 startToEnd = Vector3.Normalize( endPos - strtPos );

			Vector3[] crnrs = new Vector3[4] {
				strtPos + (Vector3.Cross(startToEnd, Vector3.back).normalized * thckNess * 0.5f ) + (startToEnd * thckNess), //top-left
				endPos + (Vector3.Cross(startToEnd, Vector3.back).normalized * thckNess * 0.5f ) - (startToEnd * thckNess), //top-right
				endPos + (Vector3.Cross(startToEnd, Vector3.forward).normalized * thckNess * 0.5f ), //bottom-right
				strtPos + (Vector3.Cross(startToEnd, Vector3.forward).normalized * thckNess * 0.5f ) //bottom-left
			};

			return crnrs;
		}

		public static Vector3[] GenerateRectangularFrameCorners(float lnThickness, float height_passed, float width_passed, int mode)
		{
			Vector3[] crnrs = new Vector3[4];

			if (mode == 0) //top rect
			{
				crnrs = GenerateMiteredLineCorners(
					(Vector3.up * height_passed * 0.5f) + (Vector3.right * (width_passed + lnThickness) * 0.5f),
					(Vector3.up * height_passed * 0.5f) + (Vector3.left * (width_passed + lnThickness) * 0.5f),
					lnThickness
					);
			}
			else if (mode == 1) //right rect
			{
				crnrs = GenerateMiteredLineCorners(
					(Vector3.down * (height_passed + lnThickness) * 0.5f) + (Vector3.right * width_passed * 0.5f),
					(Vector3.up * (height_passed + lnThickness) * 0.5f) + (Vector3.right * width_passed * 0.5f),
					lnThickness
				);
			}
			else if (mode == 2) //bottom rect
			{
				crnrs = GenerateMiteredLineCorners((Vector3.down * height_passed * 0.5f) + (Vector3.left * (width_passed + lnThickness) * 0.5f),
					(Vector3.down * height_passed * 0.5f) + (Vector3.right * (width_passed + lnThickness) * 0.5f),
					lnThickness
				);
			}
			else if (mode == 3) //left rect
			{
				crnrs = GenerateMiteredLineCorners(
					(Vector3.up * (height_passed + lnThickness) * 0.5f) + (Vector3.left * width_passed * 0.5f),
					(Vector3.down * (height_passed + lnThickness) * 0.5f) + (Vector3.left * width_passed * 0.5f),
					lnThickness
				);
			}

			return crnrs;
		}

		public static Vector3[] GenerateRectangularFrameCorners( Vector3 cntrPos_passed, float lnThickness, float height_passed, float width_passed, int mode )
		{
			Vector3[] crnrs = new Vector3[4];

			if( mode == 0 ) //top rect
			{
				crnrs = GenerateMiteredLineCorners(
					cntrPos_passed + (Vector3.up * height_passed * 0.5f) + (Vector3.right * (width_passed + lnThickness) * 0.5f),
					cntrPos_passed + (Vector3.up * height_passed * 0.5f) + (Vector3.left * (width_passed + lnThickness) *  0.5f),
					lnThickness
					);
			}
			else if ( mode == 1 ) //right rect
			{
				crnrs = GenerateMiteredLineCorners(
					cntrPos_passed + (Vector3.down * (height_passed + lnThickness) * 0.5f) + (Vector3.right * width_passed * 0.5f),
					cntrPos_passed + (Vector3.up * (height_passed + lnThickness) * 0.5f) + (Vector3.right * width_passed * 0.5f),
					lnThickness
				);
			}
			else if( mode == 2 ) //bottom rect
			{
				crnrs = GenerateMiteredLineCorners( cntrPos_passed + (Vector3.down * height_passed * 0.5f) + (Vector3.left * (width_passed + lnThickness) * 0.5f), 
					cntrPos_passed + (Vector3.down * height_passed * 0.5f) + (Vector3.right * (width_passed + lnThickness) * 0.5f), 
					lnThickness
				);
			}
			else if ( mode == 3 ) //left rect
			{
				crnrs = GenerateMiteredLineCorners(
					cntrPos_passed + (Vector3.up * (height_passed + lnThickness) * 0.5f) + (Vector3.left * width_passed * 0.5f),
					cntrPos_passed + (Vector3.down * (height_passed + lnThickness) * 0.5f) + (Vector3.left * width_passed * 0.5f),
					lnThickness
				);
			}

			return crnrs;
		}

		public static void DrawRectangle_absolutePositioning(Vector3[] crnrs, UnityEngine.Color col, VertexHelper vh, ref int vertexIndex)
		{
			UIVertex vtx = UIVertex.simpleVert;
			vtx.color = col;

			//top-left
			vtx.position = crnrs[0];
			vh.AddVert(vtx);

			//top-right
			vtx.position = crnrs[1];
			vh.AddVert(vtx);

			//bottom-right
			vtx.position = crnrs[2];
			vh.AddVert(vtx);

			//bottom-left
			vtx.position = crnrs[3];
			vh.AddVert(vtx);

			vh.AddTriangle(vertexIndex + 0, vertexIndex + 1, vertexIndex + 2);
			vh.AddTriangle(vertexIndex + 2, vertexIndex + 3, vertexIndex + 0);

			vertexIndex += 4;
		}

		public static void DrawRectangle_absolutePositioning( Vector3 crnrA, Vector3 crnrB, Vector3 crnrC, Vector3 crnrD, UnityEngine.Color col, VertexHelper vh, ref int vertexIndex)
		{
			UIVertex vtx = UIVertex.simpleVert;
			vtx.color = col;

			//top-left
			vtx.position = crnrA;
			vh.AddVert(vtx);

			//top-right
			vtx.position = crnrB;
			vh.AddVert(vtx);

			//bottom-right
			vtx.position = crnrC;
			vh.AddVert(vtx);

			//bottom-left
			vtx.position = crnrD;
			vh.AddVert(vtx);

			vh.AddTriangle(vertexIndex + 0, vertexIndex + 1, vertexIndex + 2);
			vh.AddTriangle(vertexIndex + 2, vertexIndex + 3, vertexIndex + 0);

			vertexIndex += 4;
		}

		public static void DrawRectangle_absolutePositioning( Vector3 boundsMin, Vector3 boundsMax, UnityEngine.Color col, VertexHelper vh, ref int vertexIndex)
		{
			DrawRectangle_absolutePositioning( 
				new Vector3(boundsMin.x, boundsMax.y), 
				boundsMax, 
				new Vector3( boundsMax.x, boundsMin.y ),
				boundsMin,
				col, vh, ref vertexIndex );
		}

		public static Vector3[] GenerateGridLineCorners( Vector3 boundsMin, Vector3 boundsMax )
		{
			Vector3[] crnrs = new Vector3[4];
			crnrs[0] = new Vector3(boundsMin.x, boundsMax.y);
			crnrs[1] = boundsMax;
			crnrs[2] = new Vector3( boundsMax.x, boundsMin.y );
			crnrs[3] = boundsMin;

			return crnrs;
		}

		public static Vector3 CornerProject(Vector3 vStart, Vector3 vBend, Vector3 vEnd, bool projectWithBend = true)
		{
			if (projectWithBend)
			{
				return Vector3.Normalize(
					Vector3.Normalize(vBend - vStart) +
					Vector3.Normalize(vBend - vEnd)
					);
			}
			else
			{
				return Vector3.Normalize(
					Vector3.Normalize(vStart - vBend) +
					Vector3.Normalize(vEnd - vBend)
				);
			}
		}

		public static Vector2 CornerProject(Vector2 vStart, Vector2 vBend, Vector2 vEnd, bool projectWithBend = true)
		{
			Vector2 vector = Vector2.zero;
			if ( projectWithBend )
			{
				vector = ( (vBend - vStart).normalized + (vBend - vEnd).normalized ).normalized;
			}
			else
			{
				vector = ( (vStart - vBend).normalized + (vEnd - vBend).normalized ).normalized;
			}

			// The following happens when the trajectory between all 3 vectors is perfectly straight, which would result in 
			// 0-magnitude vector, causing the line walls to "pinch"...
			if ( vector.magnitude <= 0f ) 
			{
				//Debug.Log("Got magnitude of 0....");
				vector = Quaternion.Euler(0f, 0f, -90) * (vBend - vStart).normalized;
			}

			return vector;
		}

		public static float GetHypotenuseLengthViaSOH( float theta, float oppositeLength ) 
		{
			return Mathf.Abs( oppositeLength / Mathf.Sin( theta * Mathf.Deg2Rad) ); //Unity's Mathf.Sin assumes radians rather than degrees, so a conversion is necessary
		}

		public static float GetHypotenuseLengthViaCAH(float theta, float adjacentLength)
		{
			return Mathf.Abs( adjacentLength / Mathf.Cos(theta * Mathf.Deg2Rad) ); //Unity's Mathf.Cos assumes radians rather than degrees, so a conversion is necessary
		}

		/// <summary>
		/// Gets the angles derived from a line pointing from the bend to the start, and a line
		/// pointing from the bend to the end.
		/// </summary>
		/// <param name="vStart">Start point of the bend</param>
		/// <param name="vBend">Mid point with the bend of interest</param>
		/// <param name="vEnd">Final point in the bend.</param>
		/// <returns>
		/// Angle in degrees(from -180 to 180). If the start point is to left of the end 
		/// point, this value will be negative if the endpoint is higher in the y than the start point.
		/// </returns>
		public static float SignedAngleAtBend( Vector3 vStart, Vector3 vBend, Vector3 vEnd )
		{
			return Vector3.SignedAngle( vStart - vBend, vEnd - vBend, Vector3.forward );
		}

#if UNITY_EDITOR
		public static void DrawCornerHandleLables( params Vector3[] args )
		{
			for ( int i = 0; i < args.Length; i++ )
			{
				Handles.Label(args[i], i.ToString() );
			}
		}
#endif
	}
}