using UnityEngine;

namespace LHM
{
	public class LHM_Object
	{
		public Color MyColor;

		public Transform FollowTransform;

		public Vector3 FollowOffset;

		public LHM_RotationMode RotationMode;
		public LHM_RotationDirection RotationDirection;
		public float RotationSpeed;
	}


	[System.Serializable]
	public class LHM_RectangleInfo : LHM_Object
	{
		//private Vector3[] corners;
		//public Vector3[] Corners => corners;

		public Vector3 CenterPosition;
		public float Height;
		public float Width;

		public Vector3[] GetCorners()
		{
			return LHM_Utilities.GenerateRectCorners( CenterPosition, Height, Width );
		}

		public Vector3[] GetCorners( Vector3 vCenter )
		{
			return LHM_Utilities.GenerateRectCorners( vCenter, Height, Width );
		}
	}

	[System.Serializable]
	public class LHM_LineInfo : LHM_Object
	{
		public Vector3 StartPos;
		public Vector3 EndPos;
		public float Thickness;

		public Vector3[] GetCorners()
		{
			return LHM_Utilities.GenerateLineCorners( StartPos, EndPos, Thickness );

		}
	}

	[System.Serializable]
	public class LHM_RectangularFrameInfo : LHM_Object
	{
		public Vector3 CenterPosition;
		public float Height;
		public float Width;
		public float LineThickness;


		public Vector3[] Corners_TopRect;

		public Vector3[] Corners_BottomRect;

		public Vector3[] Corners_LeftRect;

		public Vector3[] Corners_RightRect;


		public void GenerateCorners()
		{
			Corners_TopRect = LHM_Utilities.GenerateRectangularFrameCorners( CenterPosition, LineThickness, Height, Width, 0 );
			Corners_RightRect = LHM_Utilities.GenerateRectangularFrameCorners(CenterPosition, LineThickness, Height, Width, 1 );
			Corners_BottomRect = LHM_Utilities.GenerateRectangularFrameCorners(CenterPosition, LineThickness, Height, Width, 2 );
			Corners_LeftRect = LHM_Utilities.GenerateRectangularFrameCorners(CenterPosition, LineThickness, Height, Width, 3 );
		}

		public void GenerateCorners( Vector3 vCenter )
		{
			Corners_TopRect = LHM_Utilities.GenerateRectangularFrameCorners( vCenter, LineThickness, Height, Width, 0 );
			Corners_RightRect = LHM_Utilities.GenerateRectangularFrameCorners( vCenter, LineThickness, Height, Width, 1 );
			Corners_BottomRect = LHM_Utilities.GenerateRectangularFrameCorners( vCenter, LineThickness, Height, Width, 2 );
			Corners_LeftRect = LHM_Utilities.GenerateRectangularFrameCorners( vCenter, LineThickness, Height, Width, 3 );
		}
	}
}